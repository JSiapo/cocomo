/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import MainClass.Cocomo;

/**
 *
 * @author josel
 */
public class CocomoBasico extends Cocomo {

    public CocomoBasico(int sueldo, int KLDC, int modo) {
        super(sueldo, KLDC, modo);
        calculoCocomoBasico();
    }

    private void calculoCocomoBasico() {
        switch (modo) {
            case 1: {
                esf = Math.round(2.4 * Math.pow((double) KLDC, 1.05));
                tdes = 2.5 * Math.pow(esf, 0.38);
                costo = esf * sueldo;
            }
            break;
            case 2: {
                esf = 3 * Math.pow((double) KLDC, 1.12);
                tdes = 2.5 * Math.pow(esf, 0.35);
                costo = esf * sueldo;
            }
            break;
            default: {
                esf = 3.6 * Math.pow((double) KLDC, 1.2);
                tdes = 2.5 * Math.pow(esf, 0.32);
                costo = esf * sueldo;
            }
            break;
        }
    }
}
