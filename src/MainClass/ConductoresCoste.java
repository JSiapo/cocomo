/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainClass;

/**
 *
 * @author josel
 */
public class ConductoresCoste {
    protected double producto=1;
    protected int indRSS=2;
    protected int indTBD=1;
    protected int indCPR=2;
    protected int indRTE=0;
    protected int indRMP=0;
    protected int indVMC=1;
    protected int indTRC=1;
    protected int indCAN=2;
    protected int indEAN=2;
    protected int indCPRO=2;
    protected int indESO=2;
    protected int indELP=2;
    protected int indUTP=2;
    protected int indUHS=2;
    protected int indRPL=2;

    
    protected int indRUSE=1;
    protected int indDOC=2;
    protected int indEAPL=2;
    protected int indCPER=2;
    protected int indEPLA=2;
    protected int indDMS=2;
    
    public ConductoresCoste() {
    }

    public double getProducto() {
        return producto;
    }

    public void setProducto(double producto) {
        this.producto = producto;
    }

    public int getIndRSS() {
        return indRSS;
    }

    public void setIndRSS(int indRSS) {
        this.indRSS = indRSS;
    }

    public int getIndTBD() {
        return indTBD;
    }

    public void setIndTBD(int indTBD) {
        this.indTBD = indTBD;
    }

    public int getIndCPR() {
        return indCPR;
    }

    public void setIndCPR(int indCPR) {
        this.indCPR = indCPR;
    }

    public int getIndRTE() {
        return indRTE;
    }

    public void setIndRTE(int indRTE) {
        this.indRTE = indRTE;
    }

    public int getIndRMP() {
        return indRMP;
    }

    public void setIndRMP(int indRMP) {
        this.indRMP = indRMP;
    }

    public int getIndVMC() {
        return indVMC;
    }

    public void setIndVMC(int indVMC) {
        this.indVMC = indVMC;
    }

    public int getIndTRC() {
        return indTRC;
    }

    public void setIndTRC(int indTRC) {
        this.indTRC = indTRC;
    }

    public int getIndCAN() {
        return indCAN;
    }

    public void setIndCAN(int indCAN) {
        this.indCAN = indCAN;
    }

    public int getIndEAN() {
        return indEAN;
    }

    public void setIndEAN(int indEAN) {
        this.indEAN = indEAN;
    }

    public int getIndCPRO() {
        return indCPRO;
    }

    public void setIndCPRO(int indCPRO) {
        this.indCPRO = indCPRO;
    }

    public int getIndESO() {
        return indESO;
    }

    public void setIndESO(int indESO) {
        this.indESO = indESO;
    }

    public int getIndELP() {
        return indELP;
    }

    public void setIndELP(int indELP) {
        this.indELP = indELP;
    }

    public int getIndUTP() {
        return indUTP;
    }

    public void setIndUTP(int indUTP) {
        this.indUTP = indUTP;
    }

    public int getIndUHS() {
        return indUHS;
    }

    public void setIndUHS(int indUHS) {
        this.indUHS = indUHS;
    }

    public int getIndRPL() {
        return indRPL;
    }

    public void setIndRPL(int indRPL) {
        this.indRPL = indRPL;
    }

    public int getIndRUSE() {
        return indRUSE;
    }

    public void setIndRUSE(int indRUSE) {
        this.indRUSE = indRUSE;
    }

    public int getIndDOC() {
        return indDOC;
    }

    public void setIndDOC(int indDOC) {
        this.indDOC = indDOC;
    }

    public int getIndEAPL() {
        return indEAPL;
    }

    public void setIndEAPL(int indEAPL) {
        this.indEAPL = indEAPL;
    }

    public int getIndCPER() {
        return indCPER;
    }

    public void setIndCPER(int indCPER) {
        this.indCPER = indCPER;
    }

    public int getIndEPLA() {
        return indEPLA;
    }

    public void setIndEPLA(int indEPLA) {
        this.indEPLA = indEPLA;
    }

    public int getIndDMS() {
        return indDMS;
    }

    public void setIndDMS(int indDMS) {
        this.indDMS = indDMS;
    }
    
    
}
