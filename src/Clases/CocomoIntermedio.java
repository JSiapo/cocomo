/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import MainClass.Cocomo;

/**
 *
 * @author josel
 */
public class CocomoIntermedio extends Cocomo{
    
    private double fec;
    
    public CocomoIntermedio(int sueldo, int KLDC, int modo) {
        super(sueldo, KLDC, modo);
    }

    public void setFec(int indRSS,int indTBD, int indCPR, int indRTE, int indRMP, int indVMC, int indTRC, int indCan, int indEAN,
            int indCPRO, int indESO, int indELP, int indUTP, int indUHS, int indRPL) {
        ConductCosteInter cci = new ConductCosteInter();
        cci.setIndCAN(indCan);
        cci.setIndCPR(indCPR);
        cci.setIndCPRO(indCPRO);
        cci.setIndEAN(indEAN);
        cci.setIndELP(indELP);
        cci.setIndRMP(indRMP);
        cci.setIndRPL(indRPL);
        cci.setIndRSS(indRSS);
        cci.setIndESO(indESO);
        cci.setIndRTE(indRTE);
        cci.setIndTBD(indTBD);
        cci.setIndTRC(indTRC);
        cci.setIndUHS(indUHS);
        cci.setIndUTP(indUTP);
        cci.setIndVMC(indVMC);
        fec=cci.Factor();
        operar();
    }
    
    private int redondeo(double dato){
        int resultado=(int)dato;
        if(dato-(int)dato>=0.5){
            resultado++;
        }
        return resultado;
    }
    
    private void operar(){
        double ESF_N;
        switch(modo){
            case 0:{
                ESF_N=3.2*Math.pow(KLDC, 1.05);
                esf=redondeo(ESF_N*fec);
                tdes=redondeo(2.5*Math.pow(esf, 0.38));
            }break;
            case 1:{
                ESF_N=3.*Math.pow(KLDC, 1.12);
                esf=redondeo(ESF_N*fec);            
                tdes=redondeo(2.5*Math.pow(esf, 0.35));
            }break;
            case 2:{
                ESF_N=2.8*Math.pow(KLDC, 1.2);
                esf=redondeo(ESF_N*fec);   
                tdes=redondeo(2.5*Math.pow(esf, 0.32));
            }break;
        }
        costo=esf*sueldo;
    }

    public double getFec() {
        return fec;
    }
    
}
