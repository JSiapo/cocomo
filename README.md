# Cálculo Cocomo

- Cocomo 81
- Cocomo 81-Intermedio
- Cocomo 2
- Puntos de Función
- Puntos de Casos de Uso

## REQUERIMIENTOS

**OPENJDK11**

Linux

```bash
sudo apt install openjdk-11-jdk
```

Windows

https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html

## EJECUCION

Ejcutar el archivo .jar de la carpeta dist

### Linux

```bash
java -jar Cocomo81_Intermedio_2.jar
```

### Windows

Doble click al archivo

![Ejecucion](https://gitlab.com/JSiapo/cocomo/raw/master/Screenshots/screenshot.png)