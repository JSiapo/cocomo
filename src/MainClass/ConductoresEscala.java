/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainClass;

/**
 *
 * @author jose
 */
public class ConductoresEscala {
    private double suma=18.97;
    
    private int indPREC=2;
    private int indFLEX=2;
    private int indRESL=2;
    private int indTEAM=2;
    private int indPMAT=2;

    public ConductoresEscala() {
    }
    
    public int getIndPREC() {
        return indPREC;
    }

    public void setIndPREC(int indPREC) {
        this.indPREC = indPREC;
    }

    public int getIndFLEX() {
        return indFLEX;
    }

    public void setIndFLEX(int indFLEX) {
        this.indFLEX = indFLEX;
    }

    public int getIndRESL() {
        return indRESL;
    }

    public void setIndRESL(int indRESL) {
        this.indRESL = indRESL;
    }

    public int getIndTEAM() {
        return indTEAM;
    }

    public void setIndTEAM(int indTEAM) {
        this.indTEAM = indTEAM;
    }

    public int getIndPMAT() {
        return indPMAT;
    }

    public void setIndPMAT(int indPMAT) {
        this.indPMAT = indPMAT;
    }

    public double getSuma() {
        return suma;
    }
    
    public void calculoEscala(){
        suma=0;
        System.out.println("indPREC recibido"+indPREC);
        switch(indPREC){
            case 0:{
                suma+=6.20;
            }break;
            case 1:{
                suma+=4.96;
            }break;
            case 2:{
                suma+=3.72;
            }break;
            case 3:{
                suma+=2.48;
            }break;
            case 4:{
                suma+=1.24;
            }break;
            case 5: break;
        }
        System.out.println("indFLEX recibido"+indFLEX);
        switch(indFLEX){
            case 0:{
                suma+=5.07;
            }break;
            case 1:{
                suma+=4.05;
            }break;
            case 2:{
                suma+=3.04;
            }break;
            case 3:{
                suma+=2.03;
            }break;
            case 4:{
                suma+=1.01;
            }break;
            case 5: break;
        }
        System.out.println("indRESL recibido"+indRESL);
        switch(indRESL){
            case 0:{
                suma+=7.07;
            }break;
            case 1:{
                suma+=5.65;
            }break;
            case 2:{
                suma+=4.24;
            }break;
            case 3:{
                suma+=2.83;
            }break;
            case 4:{
                suma+=1.41;
            }break;
            case 5: break;
        }
        System.out.println("indTEAM recibido"+indTEAM);
        switch(indTEAM){
            case 0:{
                suma+=5.48;
            }break;
            case 1:{
                suma+=4.38;
            }break;
            case 2:{
                suma+=3.29;
            }break;
            case 3:{
                suma+=2.19;
            }break;
            case 4:{
                suma+=1.1;
            }break;
            case 5: break;
        }
        System.out.println("indPMAT recibido"+indPMAT);
        switch(indPMAT){
            case 0:{
                suma+=7.80;
            }break;
            case 1:{
                suma+=6.24;
            }break;
            case 2:{
                suma+=4.68;
            }break;
            case 3:{
                suma+=3.12;
            }break;
            case 4:{
                suma+=1.56;
            }break;
            case 5: break;
        }
    }
}
