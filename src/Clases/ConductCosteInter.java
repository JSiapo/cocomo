/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import MainClass.ConductoresCoste;

/**
 *
 * @author josel
 */
public class ConductCosteInter extends ConductoresCoste{
    public double Factor(){
        producto=1;
        switch(indRSS){            
            case 0:{
                producto*=0.75;
            }break;
            case 1:{
                producto*=0.88;
            }break;
            case 2: break;
            case 3:{
                producto*=1.15;
            }break;
            case 4:{
                producto*=1.4;
            }break;
        }
        switch(indTBD){
            case 0:{
                producto*=0.94;
            }break;
            case 1:break;
            case 2:{
                producto*=1.08;
            }break;
            case 3:{
                producto*=1.16;
            }break;
        }
        switch(indCPR){
            case 0:{
                producto*=0.70;
            }break;
            case 1:{
                producto*=0.85;
            }break;
            case 2:break;
            case 3:{
                producto*=1.15;
            }break;
            case 4:{
                producto*=1.30;
            }break;
            case 5:{
                producto*=1.65;
            }break;
        }
        switch(indRTE){
            case 0:break;
            case 1:{
                producto*=1.11;
            }break;
            case 2:{
                producto*=1.30;
            }break;
            case 3:{
                producto*=1.66;
            }break;
        }
        switch(indRMP){
            case 0:break;
            case 1:{
                producto*=1.06;
            }break;
            case 2:{
                producto*=1.30;
            }break;
            case 3:{
                producto*=1.58;
            }break;
        }
        switch(indVMC){
            case 0:{
                producto*=0.87;
            }break;
            case 1:break;
            case 2:{
                producto*=1.15;
            }break;
            case 3:{
                producto*=1.30;
            }break;
        }
        switch(indTRC){
            case 0:{
                producto*=0.87;
            }break;
            case 1:break;
            case 2:{
                producto*=1.07;
            }break;
            case 3:{
                producto*=1.15;
            }break;
        }
        switch(indCAN){
            case 0:{
                producto*=1.46;
            }break;
            case 1:{
                producto*=1.19;
            }break;
            case 2: break;
            case 3:{
                producto*=0.86;
            }break;
            case 4:{
                producto*=0.71;
            }break;
        }
        switch(indEAN){
            case 0:{
                producto*=1.29;
            }break;
            case 1:{
                producto*=1.13;
            }break;
            case 2: break;
            case 3:{
                producto*=0.91;
            }break;
            case 4:{
                producto*=0.82;
            }break;
        }
        switch(indCPRO){
            case 0:{
                producto*=1.42;
            }break;
            case 1:{
                producto*=1.17;
            }break;
            case 2: break;
            case 3:{
                producto*=0.86;
            }break;
            case 4:{
                producto*=0.70;
            }break;
        }
        switch(indESO){
            case 0:{
                producto*=1.21;
            }break;
            case 1:{
                producto*=1.12;
            }break;
            case 2: break;
            case 3:{
                producto*=0.96;
            }break;
        }
        switch(indELP){
            case 0:{
                producto*=1.4;
            }break;
            case 1:{
                producto*=1.1;
            }break;
            case 2: break;
            case 3:{
                producto*=0.95;
            }break;
        }
        switch(indUTP){
            case 0:{
                producto*=1.24;
            }break;
            case 1:{
                producto*=1.1;
            }break;
            case 2:break;
            case 3:{
                producto*=0.91;
            }break;
            case 4:{
                producto*=0.82;
            }break;
        }
        switch(indUHS){
            case 0:{
                producto*=1.24;
            }break;
            case 1:{
                producto*=1.1;
            }break;
            case 2:break;
            case 3:{
                producto*=0.91;
            }break;
            case 4:{
                producto*=0.83;
            }break;
            case 5:{
                producto*=0.70;
            }break;
        }
        switch(indRPL){
            case 0:{
                producto*=1.23;
            }break;
            case 1:{
                producto*=1.08;
            }break;
            case 2:break;
            case 3:{
                producto*=1.04;
            }break;
            case 4:{
                producto*=1.1;
            }break;
        }
        return producto;
    }
}
