/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainClass;

import javax.swing.JTextField;

/**
 *
 * @author jose
 */
public class PuntosDeFuncion {

    private int pfsa = 0;
    private int ind;
    private JTextField txtEB;
    private JTextField txtEM;
    private JTextField txtEC;
    private JTextField txtSB;
    private JTextField txtSM;
    private JTextField txtSC;
    private JTextField txtCB;
    private JTextField txtCM;
    private JTextField txtCC;
    private JTextField txtAB;
    private JTextField txtAM;
    private JTextField txtAC;
    private JTextField txtIB;
    private JTextField txtIM;
    private JTextField txtIC;

    public int getPfsa() {
        return pfsa;
    }
    
    public int getInd() {
        return ind;
    }

    public void setInd(int ind) {
        this.ind = ind;
    }

    public JTextField getTxtEB() {
        return txtEB;
    }

    public JTextField getTxtEM() {
        return txtEM;
    }

    public JTextField getTxtEC() {
        return txtEC;
    }

    public JTextField getTxtSB() {
        return txtSB;
    }

    public JTextField getTxtSM() {
        return txtSM;
    }

    public JTextField getTxtSC() {
        return txtSC;
    }

    public JTextField getTxtCB() {
        return txtCB;
    }

    public JTextField getTxtCM() {
        return txtCM;
    }

    public JTextField getTxtCC() {
        return txtCC;
    }

    public JTextField getTxtAB() {
        return txtAB;
    }

    public JTextField getTxtAM() {
        return txtAM;
    }

    public JTextField getTxtAC() {
        return txtAC;
    }

    public JTextField getTxtIB() {
        return txtIB;
    }

    public JTextField getTxtIM() {
        return txtIM;
    }

    public JTextField getTxtIC() {
        return txtIC;
    }

    
    
    
    public void sumaPFSA() {

        if (!txtEB.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtEB.getText()) * 3;
        }
        if (!txtEM.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtEM.getText()) * 4;
        }
        if (!txtEC.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtEC.getText()) * 6;
        }
        /*-----------------------------------------------*/
        if (!txtSB.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtSB.getText()) * 4;
        }
        if (!txtSM.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtSM.getText()) * 5;
        }
        if (!txtSC.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtSC.getText()) * 7;
        }
        /*-----------------------------------------------*/
        if (!txtCB.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtCB.getText()) * 3;
        }
        if (!txtCM.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtCM.getText()) * 4;
        }
        if (!txtCC.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtCC.getText()) * 6;
        }
        /*-----------------------------------------------*/
        if (!txtAB.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtAB.getText()) * 7;
        }
        if (!txtAM.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtAM.getText()) * 10;
        }
        if (!txtAC.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtAC.getText()) * 15;
        }
        /*-----------------------------------------------*/
        if (!txtIB.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtIB.getText()) * 5;
        }
        if (!txtIM.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtIM.getText()) * 7;
        }
        if (!txtIC.getText().equals("")) {
            pfsa = pfsa + Integer.parseInt(txtIC.getText()) * 10;
        }
        /*-----------------------------------------------*/
    }

    public int LDC_PF() {
        int ldc = 0;
        switch (ind) {
            case 0:
                ldc = pfsa * 40;
                break;
            case 1:
                ldc = pfsa * 71;
                break;
            case 2:
                ldc = pfsa * 49;
                break;
            case 3:
                ldc = pfsa * 32;
                break;
            case 4:
                ldc = pfsa * 91;
                break;
            case 5:
                ldc = pfsa * 128;
                break;
            case 6:
                ldc = pfsa * 64;
                break;
            case 7:
                ldc = pfsa * 128;
                break;
            case 8:
                ldc = pfsa * 29;
                break;
            case 9:
                ldc = pfsa * 19;
                break;
            case 10:
                ldc = pfsa * 91;
                break;
            case 11:
                ldc = pfsa * 29;
                break;
            case 12:
                ldc = pfsa * 320;
                break;
            case 13:
                ldc = pfsa * 213;
                break;
            case 14:
                ldc = pfsa * 64;
                break;
            case 15:
                ldc = pfsa * 105;
                break;
            case 16:
                ldc = pfsa * 34;
                break;
            case 17:
                ldc = pfsa * 53;
                break;
            case 18:
                ldc = pfsa * 80;
                break;
            case 19:
                ldc = pfsa * 40;
                break;
            case 20:
                ldc = pfsa * 23;
                break;
            case 21:
                ldc = pfsa * 36;
                break;
            case 22:
                ldc = pfsa * 91;
                break;
            case 23:
                ldc = pfsa * 49;
                break;
            case 24:
                ldc = pfsa * 16;
                break;
            case 25:
                ldc = pfsa * 64;
                break;
            case 26:
                ldc = pfsa * 32;
                break;
            case 27:
                ldc = pfsa * 34;
                break;
            case 28:
                ldc = pfsa * 20;
                break;
        }
        return ldc;
    }

    public void setTxtEB(JTextField txtEB) {
        this.txtEB = txtEB;
    }

    public void setTxtEM(JTextField txtEM) {
        this.txtEM = txtEM;
    }

    public void setTxtEC(JTextField txtEC) {
        this.txtEC = txtEC;
    }

    public void setTxtSB(JTextField txtSB) {
        this.txtSB = txtSB;
    }

    public void setTxtSM(JTextField txtSM) {
        this.txtSM = txtSM;
    }

    public void setTxtSC(JTextField txtSC) {
        this.txtSC = txtSC;
    }

    public void setTxtCB(JTextField txtCB) {
        this.txtCB = txtCB;
    }

    public void setTxtCM(JTextField txtCM) {
        this.txtCM = txtCM;
    }

    public void setTxtCC(JTextField txtCC) {
        this.txtCC = txtCC;
    }

    public void setTxtAB(JTextField txtAB) {
        this.txtAB = txtAB;
    }

    public void setTxtAM(JTextField txtAM) {
        this.txtAM = txtAM;
    }

    public void setTxtAC(JTextField txtAC) {
        this.txtAC = txtAC;
    }

    public void setTxtIB(JTextField txtIB) {
        this.txtIB = txtIB;
    }

    public void setTxtIM(JTextField txtIM) {
        this.txtIM = txtIM;
    }

    public void setTxtIC(JTextField txtIC) {
        this.txtIC = txtIC;
    }

}
