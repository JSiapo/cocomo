/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainClass;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author Jose
 */
public class ExportaExcel {

    WritableWorkbook workbook = null;
    WritableSheet sheet = null, sheet2 = null, sheet3 = null, sheet4 = null, sheet5 = null;
    WritableFont h, h1;
    WritableCellFormat hformat = null;
    WritableCellFormat hformatTitle = null;

    private boolean flag_pf, flag_c81b, flag_c81i, flag_c2, flag_m, flag_pcu;
    private double fec, costo, costom;
    private double ce;
    private int sueldo, KLDC, lda, ldm, sueldom;
    private String modo;
    private int esf, tdes, esfm;
    private DefaultTableModel modelactor;
    private DefaultTableModel modelcaso;
    private ArrayList<Integer> puntos_funcion;
    private ArrayList<Integer> condcost81i;
    private ArrayList<Integer> condcost2;
    private ArrayList<Integer> condesc;
    private ArrayList<Integer> listfct;
    private ArrayList<Integer> listfa;

    public void GenerearExcel(String ruta) {
        WorkbookSettings conf = new WorkbookSettings();
        conf.setEncoding("ISO -8859-1");

        try {
            h = new WritableFont(WritableFont.ARIAL, 11, WritableFont.NO_BOLD);
            hformat = new WritableCellFormat(h);

            h1 = new WritableFont(WritableFont.TIMES, 13, WritableFont.BOLD);
            hformatTitle = new WritableCellFormat(h1);
            int cantidadHojas = 0;
            workbook = Workbook.createWorkbook(new File(ruta), conf);
            if (flag_pf) {
                sheet = workbook.createSheet("Puntos de Función", cantidadHojas++);
                reportarPF();
            }
            if (flag_c81b) {
                sheet2 = workbook.createSheet("Resultados Cocomo Básico", cantidadHojas++);
                reportarC81b();
            }
            if (flag_c81i) {
                sheet3 = workbook.createSheet("Resultados Cocomo Intermedio", cantidadHojas++);
                reportarC81i();
            }
            if (flag_c2) {
                sheet4 = workbook.createSheet("Resultados Cocomo 2", cantidadHojas++);
                reportar_C2();
            }
            if (flag_pcu) {
                sheet5 = workbook.createSheet("Puntos de caso de uso", cantidadHojas++);
                reporte_pcu();
            }

        } catch (IOException ex) {
            Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            workbook.write();
            try {
                workbook.close();
            } catch (WriteException ex) {
                Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void reportarPF() {
        try {
            sheet.addCell(new jxl.write.Label(1, 1, "Puntos de función", hformatTitle));
        } catch (WriteException ex) {
            Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
        }

        int b = 3, m = 4, c = 5;
        try {
            sheet.addCell(new jxl.write.Label(0, 2, "Entradas", hformatTitle));
            sheet.addCell(new jxl.write.Label(0, 6, "Salidas", hformatTitle));
            sheet.addCell(new jxl.write.Label(0, 10, "Interfaces", hformatTitle));
            sheet.addCell(new jxl.write.Label(0, 14, "Consultas", hformatTitle));
            sheet.addCell(new jxl.write.Label(0, 18, "Archivos", hformatTitle));
            for (int i = 0; i < 5; i++) {
                sheet.addCell(new jxl.write.Label(1, b, "Bajo", hformatTitle));
                sheet.addCell(new jxl.write.Label(1, m, "Medio", hformatTitle));
                sheet.addCell(new jxl.write.Label(1, c, "Complejo", hformatTitle));
                b += 4;
                m += 4;
                c += 4;
            }
        } catch (WriteException ex) {
            Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
        }

        int caux = 3;
        for (int i = 0; i < puntos_funcion.size(); i++) {
            try {
                sheet.addCell(new jxl.write.Number(2, caux, puntos_funcion.get(i), hformat));
                caux++;
                if (caux == 6 || caux == 10 || caux == 14 || caux == 18) {
                    caux++;
                }

            } catch (WriteException ex) {
                Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void reportarC81b() {
        System.out.println("sueldo" + sueldo + " kldc" + KLDC + " modo" + modo + " costo" + costo);
        int limit = 2;

        try {
            sheet2.addCell(new jxl.write.Label(0, 1, "Cocomo 81 Básico", hformatTitle));
            sheet2.addCell(new jxl.write.Label(0, limit + 1, "Sueldo", hformatTitle));
            sheet2.addCell(new jxl.write.Label(0, limit + 2, "Lineas de Código", hformatTitle));
            sheet2.addCell(new jxl.write.Label(0, limit + 3, "Modo", hformatTitle));

            sheet2.addCell(new jxl.write.Number(1, limit + 1, sueldo, hformat));
            sheet2.addCell(new jxl.write.Number(1, limit + 2, KLDC * 1000, hformat));
            sheet2.addCell(new jxl.write.Label(1, limit + 3, modo, hformat));

            sheet2.addCell(new jxl.write.Label(0, limit + 5, "Costo Proyecto", hformatTitle));
            sheet2.addCell(new jxl.write.Label(0, limit + 6, "Esfuerzo", hformatTitle));
            sheet2.addCell(new jxl.write.Label(0, limit + 7, "Tiempo", hformatTitle));

            sheet2.addCell(new jxl.write.Number(1, limit + 5, costo, hformat));
            sheet2.addCell(new jxl.write.Number(1, limit + 6, esf, hformat));
            sheet2.addCell(new jxl.write.Number(1, limit + 7, tdes, hformat));

            if (flag_m) {
                sheet2.addCell(new jxl.write.Label(0, limit + 9, "Lineas agregadas", hformatTitle));
                sheet2.addCell(new jxl.write.Label(0, limit + 10, "Lineas modificadas", hformatTitle));
                sheet2.addCell(new jxl.write.Label(0, limit + 11, "Sueldo mantenedor", hformatTitle));
                sheet2.addCell(new jxl.write.Label(0, limit + 12, "Esfuerzo mantenimiento", hformatTitle));
                sheet2.addCell(new jxl.write.Label(0, limit + 13, "Costo mantenimiento", hformatTitle));

                sheet2.addCell(new jxl.write.Number(1, limit + 9, lda, hformat));
                sheet2.addCell(new jxl.write.Number(1, limit + 10, ldm, hformat));
                sheet2.addCell(new jxl.write.Number(1, limit + 11, sueldom, hformat));
                sheet2.addCell(new jxl.write.Number(1, limit + 12, esfm, hformat));
                sheet2.addCell(new jxl.write.Number(1, limit + 13, costom, hformat));
            }
        } catch (WriteException ex) {
            Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void reportarC81i() {
        try {
            sheet3.addCell(new jxl.write.Label(0, 1, "Conductores de Costo - Cocomo 81 Intermedio", hformatTitle));

            sheet3.addCell(new jxl.write.Label(0, 3, "Requerimiento de seguridad del software", hformatTitle));
            switch (condcost81i.get(0)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 3, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 3, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 3, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 3, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet3.addCell(new jxl.write.Label(1, 3, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 4, "Tamaño de la Base de Datos", hformatTitle));
            switch (condcost81i.get(1)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 4, "Bajo", hformat));
                }
                break;
                case 1:
                    sheet3.addCell(new jxl.write.Label(1, 4, "Nominal", hformat));
                    break;
                case 2: {
                    sheet3.addCell(new jxl.write.Label(1, 4, "Alto", hformat));
                }
                break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 4, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 5, "Complejidad", hformatTitle));
            switch (condcost81i.get(2)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 5, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 5, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 5, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 5, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet3.addCell(new jxl.write.Label(1, 5, "Muy Alto", hformat));
                }
                break;
                case 5: {
                    sheet3.addCell(new jxl.write.Label(1, 5, "Extra Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 6, "Restricción de Tiempo de Ejecución", hformatTitle));
            switch (condcost81i.get(3)) {
                case 0:
                    sheet3.addCell(new jxl.write.Label(1, 6, "Nominal", hformat));
                    break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 6, "Alto", hformat));
                }
                break;
                case 2: {
                    sheet3.addCell(new jxl.write.Label(1, 6, "Muy Alto", hformat));
                }
                break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 6, "Extra Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 7, "Restricción de Memoria Principal", hformatTitle));
            switch (condcost81i.get(4)) {
                case 0:
                    sheet3.addCell(new jxl.write.Label(1, 7, "Nominal", hformat));
                    break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 7, "Alto", hformat));
                }
                break;
                case 2: {
                    sheet3.addCell(new jxl.write.Label(1, 7, "Muy Alto", hformat));
                }
                break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 7, "Extra Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 8, "Velocidad de Cambio", hformatTitle));
            switch (condcost81i.get(5)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 8, "Bajo", hformat));
                }
                break;
                case 1:
                    sheet3.addCell(new jxl.write.Label(1, 8, "Nominal", hformat));
                    break;
                case 2: {
                    sheet3.addCell(new jxl.write.Label(1, 8, "Alto", hformat));
                }
                break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 8, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 9, "Tiempo de Respuesta", hformatTitle));
            switch (condcost81i.get(6)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 9, "Bajo", hformat));
                }
                break;
                case 1:
                    sheet3.addCell(new jxl.write.Label(1, 9, "Nominal", hformat));
                    break;
                case 2: {
                    sheet3.addCell(new jxl.write.Label(1, 9, "Alto", hformat));
                }
                break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 9, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 10, "Capacidad de los analistas", hformatTitle));
            switch (condcost81i.get(7)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 10, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 10, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 10, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 10, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet3.addCell(new jxl.write.Label(1, 10, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 11, "Experiencia de los analistas", hformatTitle));
            switch (condcost81i.get(8)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 11, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 11, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 11, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 11, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet3.addCell(new jxl.write.Label(1, 11, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 12, "Capacidad de los programadores", hformatTitle));
            switch (condcost81i.get(9)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 12, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 12, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 12, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 12, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet3.addCell(new jxl.write.Label(1, 12, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 13, "Experiencia con el O.S.", hformatTitle));
            switch (condcost81i.get(10)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 13, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 13, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 13, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 13, "Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 14, "Experiencia con el L.P.", hformatTitle));
            switch (condcost81i.get(11)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 14, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 14, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 14, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 14, "Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 15, "Uso de Técnicas de Programación", hformatTitle));
            switch (condcost81i.get(12)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 15, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 15, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 15, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 15, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet3.addCell(new jxl.write.Label(1, 15, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 16, "Uso de Herramientas software", hformatTitle));
            switch (condcost81i.get(13)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 16, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 16, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 16, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 16, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet3.addCell(new jxl.write.Label(1, 16, "Muy Alto", hformat));
                }
                break;
                case 5: {
                    sheet3.addCell(new jxl.write.Label(1, 16, "Extra Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 17, "Requisitos de planificación", hformatTitle));
            switch (condcost81i.get(14)) {
                case 0: {
                    sheet3.addCell(new jxl.write.Label(1, 17, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet3.addCell(new jxl.write.Label(1, 17, "Bajo", hformat));
                }
                break;
                case 2:
                    sheet3.addCell(new jxl.write.Label(1, 17, "Nominal", hformat));
                    break;
                case 3: {
                    sheet3.addCell(new jxl.write.Label(1, 17, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet3.addCell(new jxl.write.Label(1, 17, "Muy Alto", hformat));
                }
                break;
            }
            sheet3.addCell(new jxl.write.Label(0, 19, "FEC", hformatTitle));
            sheet3.addCell(new jxl.write.Number(1, 19, fec, hformat));

            int limit = 19;

            sheet3.addCell(new jxl.write.Label(0, limit + 1, "Sueldo", hformatTitle));
            sheet3.addCell(new jxl.write.Label(0, limit + 2, "Lineas de Código", hformatTitle));
            sheet3.addCell(new jxl.write.Label(0, limit + 3, "Modo", hformatTitle));

            sheet3.addCell(new jxl.write.Number(1, limit + 1, sueldo, hformat));
            sheet3.addCell(new jxl.write.Number(1, limit + 2, KLDC * 1000, hformat));
            sheet3.addCell(new jxl.write.Label(1, limit + 3, modo, hformat));

            sheet3.addCell(new jxl.write.Label(0, limit + 5, "Costo Proyecto", hformatTitle));
            sheet3.addCell(new jxl.write.Label(0, limit + 6, "Esfuerzo", hformatTitle));
            sheet3.addCell(new jxl.write.Label(0, limit + 7, "Tiempo", hformatTitle));

            sheet3.addCell(new jxl.write.Number(1, limit + 5, costo, hformat));
            sheet3.addCell(new jxl.write.Number(1, limit + 6, esf, hformat));
            sheet3.addCell(new jxl.write.Number(1, limit + 7, tdes, hformat));

            if (flag_m) {
                sheet3.addCell(new jxl.write.Label(0, limit + 9, "Lineas agregadas", hformatTitle));
                sheet3.addCell(new jxl.write.Label(0, limit + 10, "Lineas modificadas", hformatTitle));
                sheet3.addCell(new jxl.write.Label(0, limit + 11, "Sueldo mantenedor", hformatTitle));
                sheet3.addCell(new jxl.write.Label(0, limit + 12, "Esfuerzo mantenimiento", hformatTitle));
                sheet3.addCell(new jxl.write.Label(0, limit + 13, "Costo mantenimiento", hformatTitle));

                sheet3.addCell(new jxl.write.Number(1, limit + 9, lda, hformat));
                sheet3.addCell(new jxl.write.Number(1, limit + 10, ldm, hformat));
                sheet3.addCell(new jxl.write.Number(1, limit + 11, sueldom, hformat));
                sheet3.addCell(new jxl.write.Number(1, limit + 12, esfm, hformat));
                sheet3.addCell(new jxl.write.Number(1, limit + 13, costom, hformat));
            }

        } catch (WriteException ex) {
            Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void reportar_C2() {
        System.out.println("Conductores de coste cocomo 2");
        try {
            sheet4.addCell(new jxl.write.Label(0, 1, "Conductore de coste - Cocomo 2", hformatTitle));

            sheet4.addCell(new jxl.write.Label(0, 3, "Requerimiento de seguridad del software", hformatTitle));
            //System.out.println("indRSS es "+Main.cc2.getIndRSS());
            switch (condcost2.get(0)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 3, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 3, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 3, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 3, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 3, "Muy Alto", hformat));
                }
                break;
            }

            sheet4.addCell(new jxl.write.Label(0, 4, "Tamaño de la Base de Datos", hformatTitle));
            //System.out.println("indTBD es "+Main.cc2.getIndTBD());
            switch (condcost2.get(1)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 4, "Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 4, "Nominal", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 4, "Alto", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 4, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 5, "Complejidad", hformatTitle));
            //System.out.println("indCPR es "+Main.cc2.getIndCPR());
            switch (condcost2.get(2)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 5, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 5, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 5, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 5, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 5, "Muy Alto", hformat));
                }
                break;
                case 5: {
                    sheet4.addCell(new jxl.write.Label(1, 5, "Extra Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 6, "Reutilización", hformatTitle));
            //System.out.println("indRUSE es "+Main.cc2.getIndRUSE());
            switch (condcost2.get(3)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 6, "Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 6, "Nominal", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 6, "Alto", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 6, "Muy Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 6, "Extra Alto", hformat));
                }
                break;
            }
            //System.out.println("indDOC es "+Main.cc2.getIndDOC());
            sheet4.addCell(new jxl.write.Label(0, 7, "Documentación", hformatTitle));
            switch (condcost2.get(4)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 7, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 7, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 7, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 7, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 7, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 8, "Restricción de Tiempo de Ejecución", hformatTitle));
            //System.out.println("indRTE es "+Main.cc2.getIndRTE());
            switch (condcost2.get(5)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 8, "Nominal", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 8, "Alto", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 8, "Muy Alto", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 8, "Extra Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 9, "Restricción de Memoria Principal", hformatTitle));
            //System.out.println("indRMP es "+Main.cc2.getIndRMP());
            switch (condcost2.get(6)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 9, "Nominal", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 9, "Alto", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 9, "Muy Alto", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 9, "Extra Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 10, "Velocidad de Cambio", hformatTitle));
            switch (condcost2.get(7)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 10, "Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 10, "Nominal", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 10, "Alto", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 10, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 11, "Capacidad de los analistas", hformatTitle));
            switch (condcost2.get(8)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 11, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 11, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 11, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 11, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 11, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 12, "Experiencia en la aplicacion", hformatTitle));
            switch (condcost2.get(9)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 12, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 12, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 12, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 12, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 12, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 13, "Capacidad de los programadores", hformatTitle));
            switch (condcost2.get(10)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 13, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 13, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 13, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 13, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 13, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 14, "Continuidad del Personal", hformatTitle));
            switch (condcost2.get(11)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 14, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 14, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 14, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 14, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 14, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 15, "Experiencia en la plataforma", hformatTitle));
            switch (condcost2.get(12)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 15, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 15, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 15, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 15, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 15, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 16, "Experiencia con el Lenguaje de programación", hformatTitle));
            switch (condcost2.get(13)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 16, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 16, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 16, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 16, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 16, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 17, "Uso de Herramientas software", hformatTitle));
            switch (condcost2.get(14)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 17, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 17, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 17, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 17, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 17, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 18, "Requisitos de planificación", hformatTitle));
            switch (condcost2.get(15)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 18, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 18, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 18, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 18, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 18, "Muy Alto", hformat));
                }
                break;
            }
            sheet4.addCell(new jxl.write.Label(0, 19, "Requisitos de planificación", hformatTitle));
            switch (condcost2.get(16)) {
                case 0: {
                    sheet4.addCell(new jxl.write.Label(1, 19, "Muy Bajo", hformat));
                }
                break;
                case 1: {
                    sheet4.addCell(new jxl.write.Label(1, 19, "Bajo", hformat));
                }
                break;
                case 2: {
                    sheet4.addCell(new jxl.write.Label(1, 19, "Nominal", hformat));
                }
                break;
                case 3: {
                    sheet4.addCell(new jxl.write.Label(1, 19, "Alto", hformat));
                }
                break;
                case 4: {
                    sheet4.addCell(new jxl.write.Label(1, 19, "Muy Alto", hformat));
                }
                break;
                case 5: {
                    sheet4.addCell(new jxl.write.Label(1, 19, "Extra Alto", hformat));
                }
                break;
            }

            sheet4.addCell(new jxl.write.Label(0, 21, "FEC", hformatTitle));
            sheet4.addCell(new jxl.write.Number(1, 21, fec, hformat));

            int limit = 23;

            String cadena[] = {"Muy Bajo", "Bajo", "Nominal", "Alto", "Muy Alto", "Extra Alto"};
            sheet4.addCell(new jxl.write.Label(0, limit+1, "Conductores de Escala", hformatTitle));
            sheet4.addCell(new jxl.write.Label(0, limit+3, "Precedentes", hformatTitle));
            sheet4.addCell(new jxl.write.Label(1, limit+3, cadena[condesc.get(0)], hformat));
            sheet4.addCell(new jxl.write.Label(0, limit+4, "Flexibilidad de Desarrollo", hformatTitle));
            sheet4.addCell(new jxl.write.Label(1, limit+4, cadena[condesc.get(1)], hformat));
            sheet4.addCell(new jxl.write.Label(0, limit+5, "Resolución de Arquitectura/riesgo", hformatTitle));
            sheet4.addCell(new jxl.write.Label(1, limit+5, cadena[condesc.get(2)], hformat));
            sheet4.addCell(new jxl.write.Label(0, limit+6, "Cohesión del equipo", hformatTitle));
            sheet4.addCell(new jxl.write.Label(1, limit+6, cadena[condesc.get(3)], hformat));
            sheet4.addCell(new jxl.write.Label(0, limit+7, "Madurez del Proceso", hformatTitle));
            sheet4.addCell(new jxl.write.Label(1, limit+7, cadena[condesc.get(4)], hformat));
            sheet4.addCell(new jxl.write.Label(0, limit+9, "SE", hformatTitle));
            sheet4.addCell(new jxl.write.Number(1, limit+9, ce, hformat));
            
            limit=limit+9;
            
            sheet4.addCell(new jxl.write.Label(0, limit + 1, "Sueldo", hformatTitle));
            sheet4.addCell(new jxl.write.Label(0, limit + 2, "Lineas de Código", hformatTitle));
            sheet4.addCell(new jxl.write.Label(0, limit + 3, "Modo", hformatTitle));

            sheet4.addCell(new jxl.write.Number(1, limit + 1, sueldo, hformat));
            sheet4.addCell(new jxl.write.Number(1, limit + 2, KLDC * 1000, hformat));
            sheet4.addCell(new jxl.write.Label(1, limit + 3, modo, hformat));

            sheet4.addCell(new jxl.write.Label(0, limit + 5, "Costo Proyecto", hformatTitle));
            sheet4.addCell(new jxl.write.Label(0, limit + 6, "Esfuerzo", hformatTitle));
            sheet4.addCell(new jxl.write.Label(0, limit + 7, "Tiempo", hformatTitle));

            sheet4.addCell(new jxl.write.Number(1, limit + 5, costo, hformat));
            sheet4.addCell(new jxl.write.Number(1, limit + 6, esf, hformat));
            sheet4.addCell(new jxl.write.Number(1, limit + 7, tdes, hformat));

            if (flag_m) {
                sheet4.addCell(new jxl.write.Label(0, limit + 9, "Lineas agregadas", hformatTitle));
                sheet4.addCell(new jxl.write.Label(0, limit + 10, "Lineas modificadas", hformatTitle));
                sheet4.addCell(new jxl.write.Label(0, limit + 11, "Sueldo mantenedor", hformatTitle));
                sheet4.addCell(new jxl.write.Label(0, limit + 12, "Esfuerzo mantenimiento", hformatTitle));
                sheet4.addCell(new jxl.write.Label(0, limit + 13, "Costo mantenimiento", hformatTitle));

                sheet4.addCell(new jxl.write.Number(1, limit + 9, lda, hformat));
                sheet4.addCell(new jxl.write.Number(1, limit + 10, ldm, hformat));
                sheet4.addCell(new jxl.write.Number(1, limit + 11, sueldom, hformat));
                sheet4.addCell(new jxl.write.Number(1, limit + 12, esfm, hformat));
                sheet4.addCell(new jxl.write.Number(1, limit + 13, costom, hformat));
            }
        } catch (WriteException ex) {
            Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void reporte_pcu(){
        try {
            sheet5.addCell(new jxl.write.Label(0,1,"Puntos de casos de uso",hformatTitle));
            sheet5.addCell(new jxl.write.Label(0,3,"Actores",hformatTitle));
            int row=3;
            for(int i=0;i<modelactor.getRowCount();i++){
                sheet5.addCell(new jxl.write.Label(0,row+1,modelactor.getValueAt(i, 0).toString(),hformat));
                sheet5.addCell(new jxl.write.Label(1,row+1,modelactor.getValueAt(i, 1).toString(),hformat));
                sheet5.addCell(new jxl.write.Label(2,row+1,modelactor.getValueAt(i, 2).toString(),hformat));
                sheet5.addCell(new jxl.write.Label(3,row+1,modelactor.getValueAt(i, 3).toString(),hformat));
                row++;
            }
            sheet5.addCell(new jxl.write.Label(0,row++,"Casos",hformatTitle));
            for(int i=0;i<modelcaso.getRowCount();i++){
                sheet5.addCell(new jxl.write.Label(0,row+1,modelcaso.getValueAt(i, 0).toString(),hformat));
                sheet5.addCell(new jxl.write.Label(1,row+1,modelcaso.getValueAt(i, 1).toString(),hformat));
                sheet5.addCell(new jxl.write.Label(2,row+1,modelcaso.getValueAt(i, 2).toString(),hformat));
                row++;
            }
            row=5+modelcaso.getRowCount()+modelactor.getRowCount();
            row=row+3;
            sheet5.addCell(new jxl.write.Label(0,row-1,"Factores de complejidad técnica",hformatTitle));
            sheet5.addCell(new jxl.write.Label(0,row,"Sistema Distribuido",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(0),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Desempeño",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(1),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Eficiencia del usuario final (en-linea)",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(2),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Complejidad del procesamiento interno ",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(3),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Reusabilidad del código ",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(4),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Facilidad de instalación ",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(5),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Facilidad de uso ",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(6),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Portabilidad",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(7),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Facilidad de cambio",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(8),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Concurrencia ",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(9),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Características especiales de seguridad",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(10),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Provee acceso a terceros",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(11),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Facilidades especiales de entrenamiento a los usuarios",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfct.get(12),hformat));
            row=row+3;
            sheet5.addCell(new jxl.write.Label(0,row-1,"Factores de ambiente",hformatTitle));
            sheet5.addCell(new jxl.write.Label(0,row,"Familiaridad con el proceso unificado de Rational",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfa.get(0),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Experiencia en el desarrollo de aplicaciones",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfa.get(1),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Experiencia en orientación a objetos",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfa.get(2),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Capacidad del jefe de proyecto",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfa.get(3),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Motivación ",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfa.get(4),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Estabilidad de los requerimientos ",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfa.get(5),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Personal a tiempo parcial",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfa.get(6),hformat));
            sheet5.addCell(new jxl.write.Label(0,row,"Lenguaje de programación dificil",hformatTitle));
            sheet5.addCell(new jxl.write.Number(1,row++,listfa.get(7),hformat));
            
            
        } catch (WriteException ex) {
            Logger.getLogger(ExportaExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void setFlag_pf(boolean flag_pf) {
        this.flag_pf = flag_pf;
    }

    public void setFlag_c81b(boolean flag_c81b) {
        this.flag_c81b = flag_c81b;
    }

    public void setFlag_c81i(boolean flag_c81i) {
        this.flag_c81i = flag_c81i;
    }

    public void setFlag_pcu(boolean flag_pcu) {
        this.flag_pcu = flag_pcu;
    }

    public void setFlag_c2(boolean flag_c2) {
        this.flag_c2 = flag_c2;
    }

    public void setPuntos_funcion(ArrayList<Integer> puntos_funcion) {
        this.puntos_funcion = puntos_funcion;
    }

    public void setFlag_m(boolean flag_m) {
        this.flag_m = flag_m;
    }

    public void setCostom(double costom) {
        this.costom = costom;
    }

    public void setLda(int lda) {
        this.lda = lda;
    }

    public void setLdm(int ldm) {
        this.ldm = ldm;
    }

    public void setSueldom(int sueldom) {
        this.sueldom = sueldom;
    }

    public void setEsfm(int esfm) {
        this.esfm = esfm;
    }

    public void setCondcost81i(ArrayList<Integer> condcost81i) {
        this.condcost81i = condcost81i;
    }

    public void setFec(double fec) {
        this.fec = fec;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public void setSueldo(int sueldo) {
        this.sueldo = sueldo;
    }

    public void setKLDC(int KLDC) {
        this.KLDC = KLDC;
    }

    public void setModo(String modo) {
        this.modo = modo;
    }

    public void setEsf(int esf) {
        this.esf = esf;
    }

    public void setTdes(int tdes) {
        this.tdes = tdes;
    }

    public void setCe(double ce) {
        this.ce = ce;
    }

    public void setCondcost2(ArrayList<Integer> condcost2) {
        this.condcost2 = condcost2;
    }

    public void setCondesc(ArrayList<Integer> condesc) {
        this.condesc = condesc;
    }

    public void setModelactor(DefaultTableModel modelactor) {
        this.modelactor = modelactor;
    }

    public void setModelcaso(DefaultTableModel modelcaso) {
        this.modelcaso = modelcaso;
    }

    public void setListfct(ArrayList<Integer> listfct) {
        this.listfct = listfct;
    }

    public void setListfa(ArrayList<Integer> listfa) {
        this.listfa = listfa;
    }

}
