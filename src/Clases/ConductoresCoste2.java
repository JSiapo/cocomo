/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import MainClass.ConductoresCoste;

/**
 *
 * @author jose
 */
public class ConductoresCoste2 extends ConductoresCoste{

    public double cc(){
        producto=1;
        switch(indRSS){            
            case 0:{
                producto*=0.82;
            }break;
            case 1:{
                producto*=0.92;
            }break;
            case 2: break;
            case 3:{
                producto*=1.10;
            }break;
            case 4:{
                producto*=1.26;
            }break;
        }
        switch(indTBD){
            case 0:{
                producto*=0.9;
            }break;
            case 1:break;
            case 2:{
                producto*=1.14;
            }break;
            case 3:{
                producto*=1.28;
            }break;
        }
        switch(indCPR){
            case 0:{
                producto*=0.73;
            }break;
            case 1:{
                producto*=0.87;
            }break;
            case 2:break;
            case 3:{
                producto*=1.17;
            }break;
            case 4:{
                producto*=1.34;
            }break;
            case 5:{
                producto*=1.74;
            }break;
        }
        switch(indRUSE){            
            case 0:{
                producto*=0.95;
            }break;
            case 1: break;
            case 2:{
                producto*=1.07;
            }break;
            case 3:{
                producto*=1.15;
            }break;
            case 4:{
                producto*=1.24;
            }break;
        }
        switch(indDOC){            
            case 0:{
                producto*=0.81;
            }break;
            case 1:{
                producto*=0.91;
            }break;
            case 2:break;
            case 3:{
                producto*=1.11;
            }break;
            case 4:{
                producto*=1.23;
            }break;
        }
        switch(indRTE){
            case 0:break;
            case 1:{
                producto*=1.11;
            }break;
            case 2:{
                producto*=1.29;
            }break;
            case 3:{
                producto*=1.63;
            }break;
        }
        switch(indRMP){
            case 0:break;
            case 1:{
                producto*=1.05;
            }break;
            case 2:{
                producto*=1.17;
            }break;
            case 3:{
                producto*=1.46;
            }break;
        }
        switch(indVMC){
            case 0:{
                producto*=0.87;
            }break;
            case 1:break;
            case 2:{
                producto*=1.15;
            }break;
            case 3:{
                producto*=1.30;
            }break;
        }
        switch(indCAN){
            case 0:{
                producto*=1.42;
            }break;
            case 1:{
                producto*=1.19;
            }break;
            case 2: break;
            case 3:{
                producto*=0.85;
            }break;
            case 4:{
                producto*=0.71;
            }break;
        }
        switch(indEAPL){
            case 0:{
                producto*=1.22;
            }break;
            case 1:{
                producto*=1.1;
            }break;
            case 2: break;
            case 3:{
                producto*=0.88;
            }break;
            case 4:{
                producto*=0.81;
            }break;
        }
        switch(indCPRO){
            case 0:{
                producto*=1.34;
            }break;
            case 1:{
                producto*=1.15;
            }break;
            case 2: break;
            case 3:{
                producto*=0.88;
            }break;
            case 4:{
                producto*=0.76;
            }break;
        }
        switch(indCPER){
            case 0:{
                producto*=1.29;
            }break;
            case 1:{
                producto*=1.12;
            }break;
            case 2: break;
            case 3:{
                producto*=0.9;
            }break;
            case 4:{
                producto*=0.81;
            }break;
        }
        switch(indEPLA){
            case 0:{
                producto*=1.19;
            }break;
            case 1:{
                producto*=1.09;
            }break;
            case 2: break;
            case 3:{
                producto*=0.91;
            }break;
            case 4:{
                producto*=0.85;
            }break;
        }
        switch(indELP){
            case 0:{
                producto*=1.2;
            }break;
            case 1:{
                producto*=1.09;
            }break;
            case 2: break;
            case 3:{
                producto*=0.91;
            }break;
            case 4:{
                producto*=0.84;
            }break;
        }
        switch(indUHS){
            case 0:{
                producto*=1.17;
            }break;
            case 1:{
                producto*=1.09;
            }break;
            case 2: break;
            case 3:{
                producto*=0.9;
            }break;
            case 4:{
                producto*=0.78;
            }break;
        }
        switch(indRPL){
            case 0:{
                producto*=1.43;
            }break;
            case 1:{
                producto*=1.14;
            }break;
            case 2: break;
            case 3: break;
            case 4: break;
        }
        switch(indDMS){
            case 0:{
                producto*=1.22;
            }break;
            case 1:{
                producto*=1.09;
            }break;
            case 2: break;
            case 3:{
                producto*=0.93;
            }break;
            case 4:{
                producto*=0.86;
            }break;
            case 5:{
                producto*=0.8;
            }break;
        }
        return producto;
    }
    
}
