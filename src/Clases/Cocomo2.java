/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Clases;

import MainClass.ConductoresEscala;
import MainClass.Cocomo;

/**
 *
 * @author josel
 */
public class Cocomo2 extends Cocomo{
    
    private double fec;
    private double ce;
    
    public Cocomo2(int sueldo, int KLDC, int modo) {
        super(sueldo, KLDC, modo);
    }
    
    public void calc_fec(int indRSS,int indTBD, int indCPR, int indRTE, int indRMP, int indVMC, int indCan,int indEPLA,
            int indCPRO, int indELP, int indUHS, int indRPL, int indRUSE, int indDOC, int indEAPL, int indCPER, int indDMS) {
        ConductoresCoste2 cc2=new ConductoresCoste2();
        cc2.setIndCAN(indCan);
        cc2.setIndCPR(indCPR);
        cc2.setIndCPRO(indCPRO);
        cc2.setIndELP(indELP);
        cc2.setIndRMP(indRMP);
        cc2.setIndRPL(indRPL);
        cc2.setIndRSS(indRSS);
        cc2.setIndRTE(indRTE);
        cc2.setIndTBD(indTBD);
        cc2.setIndUHS(indUHS);
        cc2.setIndVMC(indVMC);
        cc2.setIndRUSE(indRUSE);
        cc2.setIndDOC(indDOC);
        cc2.setIndEAPL(indEAPL);
        cc2.setIndCPER(indCPER);
        cc2.setIndEPLA(indEPLA);
        cc2.setIndDMS(indDMS);
        fec=cc2.cc();
        System.out.println("fec="+fec);
    }
    
    public void calc_ce(int indPREC, int indFLEX, int indRESL, int indTEAM, int indPMAT){
        ConductoresEscala ces=new ConductoresEscala();
        ces.setIndFLEX(indFLEX);
        ces.setIndPMAT(indPMAT);
        ces.setIndPREC(indPREC);
        ces.setIndRESL(indRESL);
        ces.setIndTEAM(indTEAM);
        ces.calculoEscala();
        ce=ces.getSuma();
        System.out.println("ce="+ce);
        operar();
    }
    
    private int redondeo(double dato){
        int resultado=(int)dato;
        if(dato-(int)dato>=0.5){
            resultado++;
        }
        return resultado;
    }
    
    private void operar(){
        esf=redondeo(2.94*fec*Math.pow((KLDC), (0.91+(ce/100))));
        tdes=redondeo(3.67*Math.pow(esf,0.28+0.2*(ce/100)));
        costo=esf*sueldo;
    }

    public double getFec() {
        return fec;
    }

    public double getCe() {
        return ce;
    }
    
    
}
