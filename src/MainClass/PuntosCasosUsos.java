/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainClass;

import java.util.ArrayList;

/**
 *
 * @author jose
 */
public class PuntosCasosUsos {
    private ArrayList<Double>valoresfct=new ArrayList<>();
    private ArrayList<Integer>inputfct;
    private double fct=0.0;
    
    private ArrayList<Double>valoresfa=new ArrayList<>();
    private ArrayList<Integer>inputfa;
    private double fa=0.0;

    private int pcusa;
    private double pcua;
    private int esf;
    
    public PuntosCasosUsos(ArrayList<Integer> inputfct, ArrayList<Integer> inputfa, int pa, int pcu) {
        this.inputfct = inputfct;
        this.inputfa=inputfa;
        this.pcusa=pa+pcu;
        initvalores();
        operar();
    }
    private void initvalores(){
        valoresfct.add(2.0);
        valoresfct.add(1.0);
        valoresfct.add(1.0);
        valoresfct.add(1.0);
        valoresfct.add(1.0);
        valoresfct.add(0.5);
        valoresfct.add(0.5);
        valoresfct.add(2.0);
        valoresfct.add(1.0);
        valoresfct.add(1.0);
        valoresfct.add(1.0);
        valoresfct.add(1.0);
        valoresfct.add(1.0);
        
        
        valoresfa.add(1.5);
        valoresfa.add(0.5);
        valoresfa.add(1.0);
        valoresfa.add(0.5);
        valoresfa.add(1.0);
        valoresfa.add(2.0);
        valoresfa.add(-1.0);
        valoresfa.add(-1.0);
        
    }
    
    private void operar(){
        for(int i=0;i<inputfct.size();i++){
            fct=fct+(inputfct.get(i)*valoresfct.get(i));
        }
        fct=0.6+(0.01*fct);
        for (int i = 0; i < inputfa.size(); i++) {
            fa=fa+(inputfa.get(i)*valoresfa.get(i));
        }
        fa=1.4+(-0.03*fa);
        
        pcua=pcusa*fct*fa;
        
        int x=0,y=0;
        for (int i = 0; i < inputfa.size()-2; i++) {
            x=inputfa.get(i)<3?x+1:x;
        }
        y=inputfa.get(6)>3?y+1:y;
        y=inputfa.get(7)>3?y+1:y;
        
        esf=x+y<=2?20:(x+y==3?28:(x+y==4?28:-1));
    }

    public double getFct() {
        return fct;
    }

    public double getFa() {
        return fa;
    }

    public int getPcusa() {
        return pcusa;
    }

    public double getPcua() {
        return pcua;
    }

    public int getEsf() {
        return esf;
    }
    
    
    
}
