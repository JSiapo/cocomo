/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainClass;

/**
 *
 * @author josel
 */
public class Cocomo {
    protected final int sueldo;
    protected final int KLDC;
    protected final int modo;
    
    protected double costo;
    protected double esf;
    protected double tdes;

    public Cocomo(int sueldo, int KLDC, int modo) {
        this.sueldo = sueldo;
        this.KLDC = KLDC;
        this.modo = modo;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public double getEsf() {
        return esf;
    }

    public void setEsf(double esf) {
        this.esf = esf;
    }

    public double getTdes() {
        return tdes;
    }

    public void setTdes(double tdes) {
        this.tdes = tdes;
    }
    
    
}
